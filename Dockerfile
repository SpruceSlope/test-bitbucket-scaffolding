FROM openjdk:11-alpine
ENTRYPOINT ["/usr/bin/test-bitbucket-scaffolding.sh"]

COPY test-bitbucket-scaffolding.sh /usr/bin/test-bitbucket-scaffolding.sh
COPY target/test-bitbucket-scaffolding.jar /usr/share/test-bitbucket-scaffolding/test-bitbucket-scaffolding.jar
